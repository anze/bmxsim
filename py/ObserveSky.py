import numpy as np
from astropy.time import Time, TimeMJD
from astropy import units as u
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, ICRS, FK5
from astropy.coordinates import get_sun
import healpy as hp

class ObserveSky:
    def __init__ (self, telescope):
        self.telescope=telescope

    def getTimeList(self, tstart=Time('2016-08-01 00:00:00')+4*u.hour, dt=1*u.s, Ns=3600):
        ## in august we are 4 hours of UTC, otherwise 5
        return [tstart+dt*i for i in range(Ns)]

    def getIntegratedSignal (self, tlist, sigslice, N, reso):
        beam=self.telescope.airyBeamImage(N, reso)
        beam/=beam.sum()
        Nside=int(np.sqrt(len(sigslice)/12))
        print Nside,'=NSIDE'
        vec2pix=lambda x,y,z:hp.vec2pix(Nside,x,y,z)
        toret=[]
        for i,t in enumerate(tlist):
            aaz=self.telescope.altaz(t)
            skyc=aaz.transform_to(FK5)
            rot=(skyc.ra.deg, skyc.dec.deg, 0.)
            proj=hp.projector.GnomonicProj(xsize = N, ysize = N, rot = rot, reso = reso)
            mp=proj.projmap(sigslice,vec2pix)
            csig=(mp*beam).sum()
            print i,csig,'\r',
            toret.append(csig)
        print
        return toret
        
