import numpy as np
import numpy.random as npr
import scipy.special as special_fun
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.mlab as mlab
from scipy.interpolate import interp1d
import pylab

NHEFFT=2048
HE11Cache={}
noise_level=0
def make_HE11_field(N,a,R,Lambda):
    ## make the coordinates for the aperature field
    ones = np.ones(N)
    inds  = (np.arange(N)+.5 - N/2.)
    X = np.outer(ones,inds) * R/(1.*N)
    Y = np.transpose(X)
    X = X
    Rad = np.sqrt(X**2. + Y**2.)
    ## make the field, assumed to be x polarized
    Ex  = 1/(np.sqrt(np.pi) * a)
    Ex = Ex * special_fun.j0(2.405 * Rad/a)
    Ex = Ex / special_fun.j1(2.405)
    ## mask off the outside and set it to zero
    outside = np.where(Rad > a)
    np.size(outside)
    Ex[outside] = 0.
    Ex_aperature = Ex / np.sqrt(np.sum(Ex**2.)) ## normalize this so that the total power is 1
    ## now FFT it
    Ex_fft = np.fft.fftshift(np.fft.fft2(Ex_aperature))
    feed_beam = np.real(Ex_fft*np.conj(Ex_fft))
    feed_beam = feed_beam / np.sum(feed_beam)*R*R 
    ## now calculate the coordinates fo the beam
    K_scale_factor = Lambda  / (R) * 180./np.pi
    magk2d = Rad/R*N * K_scale_factor
    boresite_angle = magk2d
    ##return the Ex array
    return(Ex_aperature,feed_beam,boresite_angle)

def Plot_Map(Map_to_Plot,c_min,c_max,X_width,Y_width):
    ## a fuciton to plot beams etc
    print("map mean:",np.mean(Map_to_Plot),"map rms:",np.std(Map_to_Plot))
    plt.figure()
    im = plt.imshow(Map_to_Plot, interpolation='bilinear', origin='lower',cmap=cm.RdBu_r)
    im.set_clim(c_min,c_max)
    cbar = plt.colorbar()
    im.set_extent([0,X_width,0,Y_width])
    plt.ylabel('angle $[^\circ]$')
    plt.xlabel('angle $[^\circ]$')
    cbar.set_label('Amplitude [unit]', rotation=270)
    plt.show()
    return(0)
  ###############################

def Plot_Map_remove_mean(Map_to_Plot,c_min,c_max,X_width,Y_width):
    ## a fuciton to plot beams etc
    print("map mean:",np.mean(Map_to_Plot),"map rms:",np.std(Map_to_Plot))
    plt.figure()
    im = plt.imshow(Map_to_Plot - np.mean(Map_to_Plot), interpolation='bilinear', origin='lower',cmap=cm.RdBu_r)
    im.set_clim(c_min,c_max)
    cbar = plt.colorbar()
    im.set_extent([0,X_width,0,Y_width])
    plt.ylabel('angle $[^\circ]$')
    plt.xlabel('angle $[^\circ]$')
    cbar.set_label('Amplitude [unit]', rotation=270)
    plt.show()
    return(0)
  ###############################


def simulate_beam_HE11feed(N,pix_size,Diam,F,Lambda,defocus,z_exit_aperature,X_center,Feed_radius):
    ## set up a two dimensional grid of coordinates
    ones = np.ones(N)
    inds  = (np.arange(N)+.5 - N/2.)
    X = np.outer(ones,inds) * pix_size
    Y = np.transpose(X)
    X = X + X_center
    Rad = np.sqrt((X-X_center)**2. + Y**2.)
    R = N*pix_size
    K_scale_factor = Lambda /  R * 180/np.pi 
    magk2d = Rad / R * N *  K_scale_factor
    
    ### set up the apareture field
    ap_field, ap_phase = amplitude_and_phase_from_focus_to_paraboloid_to_exit_aperature_HE11feed(F,X,Y,defocus,z_exit_aperature,Lambda,Feed_radius,X_center)
    
    outside_dish = np.where(Rad > Diam/2.)
    inside_dish = np.where(Rad < Diam/2.)
    spill_fraction = np.sum((ap_field[outside_dish])**2.) / np.sum(ap_field**2.)
    
    ap_field[outside_dish] = 0.
    ap_phase[outside_dish] = 0.

    #pylab.imshow(ap_field)
    #pylab.show()
    
    #ap_field = ap_field / np.sum(ap_field) ## normalize the aperature fields
    
    edge_taper = np.min(ap_field[inside_dish]) **2. / (np.max(ap_field[inside_dish]))**2.
    print "spill_fraction: ",spill_fraction,"edge taper",10. *np.log(edge_taper)

    ### plot the field and phase
    lo = np.floor(N/2-75)
    hi = np.floor(N/2+75)
    delta = (hi - lo)*pix_size
    ### plot the geometry of the telescope
    Z = (X**2. + Y**2.) / (4. * F)
    #plt.plot(X[inside_dish],Y[inside_dish])
    #plt.plot(0,0,"rd")
    #plt.show()
    #plt.plot(Y[inside_dish],Z[inside_dish])
    #plt.plot(0,F,"rd")
    #plt.show()
    ############################
    #plt.plot(X[inside_dish],Z[inside_dish])
    #plt.plot(0,F,"rd")
    #plt.show()
    ############################
    #Plot_Map(ap_field[lo:hi,lo:hi]**2.,np.min(ap_field**2.),np.max(ap_field**2.),delta,delta)
    #Plot_Map(ap_phase[lo:hi,lo:hi],np.min(ap_phase),np.max(ap_phase),delta,delta)
    ### total field
    c_ap_field = ap_field*np.exp(ap_phase * 1j)
    ### calcuate the beam
    beam = (np.fft.fftshift(np.fft.fft2(np.fft.fftshift(c_ap_field))))
    beam = np.real(beam * np.conj(beam))
    beam /= beam.sum()/(R**4)
    #lo = np.floor(N/2-300)
    #hi = np.floor(N/2+300)
    #delta = (hi - lo) *Lambda  / (N*pix_size) * 180./np.pi
    #Plot_Map(beam[lo:hi,lo:hi],0.,np.max(beam),delta,delta)
    ### return the beam and coordiantes
    return(magk2d,beam)
  ###############################

def amplitude_and_phase_from_focus_to_paraboloid_to_exit_aperature_HE11feed(F,X,Y,defocus,z_exit_aperature,Lambda,Feed_radius,X_center):
    ## equation describing the paraboloid
    Z = (X**2. + Y**2.) / (4. * F)
    z_exit_aperature=Z.max()
    normal_X = -X / (2.*F)
    normal_Y = -Y / (2.*F)
    normal_Z = 1
    mag_normal = np.sqrt(normal_X**2. + normal_Y**2. + normal_Z**2.)
    normal_X  = normal_X / mag_normal
    normal_Y  = normal_Y / mag_normal
    normal_Z  = normal_Z / mag_normal
    
    ## simulate the feed beam
    args=(NHEFFT,Feed_radius,100.*(NHEFFT/2048.),Lambda)
    if not HE11Cache.has_key(args):
        HE11Cache[args]=make_HE11_field(NHEFFT,Feed_radius,100.*(NHEFFT/2048.),Lambda)
    EHE11,Feed_beam,boresite_angle = HE11Cache[args]

    ## spline the beam so we can evaluate the illumination
    ## so thsi Feed_beam is (2048x2048 square. Here we take on diagonal from the middle out, i.e.
    ## from rad=0 ot max and then add 0 in front for the actuao boresite angle
    theta = np.concatenate((np.array([0.]),boresite_angle[NHEFFT/2:,NHEFFT/2]))
    theta = theta * np.pi / 180.   ## convert to radians to be consistant with the sim code
    ill_beam = Feed_beam[NHEFFT/2:,NHEFFT/2]
    ill_beam = np.concatenate((np.array([np.max(Feed_beam[NHEFFT/2:,NHEFFT/2])]),ill_beam))
    ill_beam = np.sqrt(ill_beam) ## covert to E-field units
    Feed_Beam_fun = interp1d(theta,ill_beam,kind='cubic')

    
    ## distance to the paraboloid from the focus
    vec_FtoP_X = X-defocus[0]
    vec_FtoP_Y = Y-defocus[1]
    vec_FtoP_Z = Z -F -defocus[2]
    pathlength_FtoP = np.sqrt(vec_FtoP_X**2. + vec_FtoP_Y**2. + vec_FtoP_Z**2.)
    tan_FtoP_X = vec_FtoP_X / pathlength_FtoP
    tan_FtoP_Y = vec_FtoP_Y / pathlength_FtoP
    tan_FtoP_Z = vec_FtoP_Z / pathlength_FtoP    
    ## ray trace off the parabola
    IdotN = tan_FtoP_X * normal_X + tan_FtoP_Y * normal_Y + tan_FtoP_Z * normal_Z
    tan_reflected_X = tan_FtoP_X - 2. * IdotN * normal_X
    tan_reflected_Y = tan_FtoP_Y - 2. * IdotN * normal_Y
    tan_reflected_Z = tan_FtoP_Z - 2. * IdotN * normal_Z
    ## calculate the distance to the focal surface
    affine_paramter = (z_exit_aperature - Z) / tan_reflected_Z
    ## calculate the total path length and the exit position
    total_pl = pathlength_FtoP + affine_paramter
    X_exit = X + tan_reflected_X * affine_paramter
    Y_exit = Y + tan_reflected_Y * affine_paramter
    Z_exit = Z + tan_reflected_Z * affine_paramter

    #pylab.imshow (Z_exit-z_exit_aperature)
    #pylab.colorbar()
    #pylab.show()

    #pylab.imshow (X_exit-X)
    #pylab.colorbar()
    #pylab.show()
    
    ## calculate the angle from the feed to boresite
    #sigma_feed = theta_feed_fwhp / np.sqrt(8. * np.log(2)) * np.pi / 180.
    tan_center_X = X_center-defocus[0]
    tan_center_Y = 0-defocus[1]
    tan_center_Z = (X_center**2. / (4.*F)) -F -defocus[2]
    mag_center_vec = np.sqrt(tan_center_X**2. + tan_center_Y**2. + tan_center_Z**2.)
    tan_center_X = tan_center_X / mag_center_vec
    tan_center_Y = tan_center_Y / mag_center_vec
    tan_center_Z = tan_center_Z / mag_center_vec
    ang_feed = np.arccos(tan_FtoP_X*tan_center_X + tan_FtoP_Y*tan_center_Y + tan_FtoP_Z*tan_center_Z )
    #pylab.imshow(ang_feed)
    #pylab.colorbar()
    #pylab.show()
    #print tan_center_X,tan_center_Y,tan_center_Z
    #print tan_FtoP_X[512,512],tan_FtoP_Y[512,512],tan_FtoP_Z[512,512]
    #print tan_center_X * tan_FtoP_X[512,512] + tan_center_Y * tan_FtoP_Y[512,512] + tan_center_Z * tan_FtoP_Z[512,512]
    
    #Plot_Map(ang_feed*58.,np.min(ang_feed*58.),np.max(ang_feed*58.),1024,1024)
    ## now fill in the aperatre fields
    #amplitude = np.sqrt(np.exp(-.5 * (ang_feed / sigma_feed)**2.))
    amplitude  = Feed_Beam_fun(ang_feed)
    ## now calcualte the phase
    npr.seed(10)
    if noise_level>0:
        total_pl+=npr.normal(0,noise_level,total_pl.shape)
    phase = (total_pl -F -z_exit_aperature ) * 2. * np.pi / Lambda
    ## return the total_pl (use the X,Y,Z exit later if we explore large defocus)
    return(amplitude,phase)
  ###############################
