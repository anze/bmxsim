## functions to calculate power spectrum
import healpy as hp
import numpy as np
def PowSpec_hp(imap):
    #returns healpix Cl
    Cls=hp.anafast(imap)
    els=np.array(range(1,len(Cls)+1)):
    return els,Cls

def PowSpec_flat(imap, rot = (10,0,0)):
    reso=healpy.pixelfunc.max_pixrad(nside)
    lmax=pi/reso
    N=np.pi/2/reso #90 degree
    reso=reso/np.pi*180.*60. ## is this in arcmin?
    proj=hp.projector.GnomonicProj(rot=rot, coord=None, xsize=N, ysize=N, reso=reso)
    mp=proj.projamp(imap,hp.vec2pix)
    return calculate_2d_spectrum(mp,delta_ell=5,ell_max=lmax,pix_size=reso,N=N):


def cosine_window(N):
    "makes a cosine window for apodizing to avoid edges effects in the 2d FFT" 
    # make a 2d coordinate system
    ones = np.ones(N)
    inds  = (np.arange(N)+.5 - N/2.)/N *np.pi ## eg runs from -pi/2 to pi/2
    X = np.outer(ones,inds)
    Y = np.transpose(X)
  
    # make a window map
    window_map = (np.cos(X) * np.cos(Y))**.5
   
    # return the window map
    return(window_map)
  ###############################
    


def calculate_2d_spectrum(Map_2d,delta_ell,ell_max,pix_size,N):
    "calcualtes the power spectrum of a 2d map by FFTing, squaring, and azimuthally averaging"
    
    # make a 2d ell coordinate system
    ones = np.ones(N)
    inds  = (np.arange(N)+.5 - N/2.) /(N-1.)
    kX = np.outer(ones,inds) / (pix_size/60. * np.pi/180.)
    kY = np.transpose(kX)
    K = np.sqrt(kX**2. + kY**2.)
    ell_scale_factor = 2. * np.pi 
    ell2d = K * ell_scale_factor
    
    # make an array to hold the power spectrum results
    N_bins = int(ell_max/delta_ell)
    ell_array = np.arange(N_bins)
    CL_array = np.zeros(N_bins)
    
    ## get the 2d fft of the map
    window = cosine_window(N)
    FMap_2d = np.fft.fftshift(np.fft.fft2(Map_2d * window))
    # get the 2d fourier transform of the map
    PSMap = (np.real(np.conj(FMap_2d) * FMap_2d))
    # fill out the spectra
    i = 0
    while (i < N_bins):
        ell_array[i] = (i + 0.5) * delta_ell
        inds_in_bin = ((ell2d >= (i* delta_ell)) * (ell2d < ((i+1)* delta_ell))).nonzero()
        CL_array[i] = np.mean(PSMap[inds_in_bin])
        i = i + 1
 
    # return the power spectrum and ell bins
    return(ell_array,CL_array*np.sqrt(pix_size /60.* np.pi/180.)*2.*1e-6)

