import BeamSim as bs
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.mlab as mlab
from scipy.special import jn
import astropy
from astropy import units as u
from astropy.time import Time, TimeMJD
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, ICRS, FK5
from astropy.coordinates import get_sun
from scipy.special import jn
from astropy.coordinates.angles import Angle


def BMXDemo_SDT():
    """ Returns a Single Diesh Telescope Object appropriate for a demonstrator."""
    pars={
        "name": "BMXDemo",
        "diam":4, # in meters
        "x_center" : 3, ## offset of the x-center from the focal plane
        "fnumber":0.8,
        "feed_a":0.75,
        "feed_spacing" : 0.75 * 2.05,
        "Nfeeds1" : 1, ## number of feeds in on direction
        "freq" : (800.,1200.),
        "alt":90,  #degrees*u.deg
        "az":180  #degrees*u.deg
    }
    return SingleDishTelescope (pars)

def BMX_SDT():
    """ Returns a Single Dish Telescope Object appropriate for something more realistic experiment."""
    pars={
        "name": "BMXD",
        "diam":1000, # in meters
        "X_center" : 75,
        "fnumber":0.8,
        "feed_a":0.75,
        "feed_spacing" : 0.75 * 2.05,
        "Nfeeds1" : 5, ## number of feeds in on direction
        "freq" : (200.,1200.)
        "alt":90,  #degrees
        "az":180,  #degrees
    }
    return SingleDishTelescope (pars)

class SingleDishTelescope(object):
    
    def __init__(self,pars):
        self.__dict__.update(pars)

        ## pix sizes of the beam from the horn and that of on-sky relfection
        self.beam_pix_size_arcmin=5
        self.pix_size=0.5
        nominal_z=(self.freq[0]+self.freq[1])/2
        self.nz=nominal_z
        self.F = self.fnumer*self.diam
        self.z_exit_aperature = 25.


        ## derived parameters
        #wavelength
        self.Lambda = self.Lambda_z(self.nz)
        # calcualte the pixel size on the dish to match the desired gridding in angular space
        self.pix_size_ang = beam_pix_size_arcmin *(1./60. * np.pi/180.)  ## expressed in radians
        self.N=1024 ##FFT
        self.pix_size_x = self.Lambda  / (self.pix_size_ang) / self.N

        if (self.name):
            print "Experiment:",name
        print "Diameter(m):",self.diam
        print "f/#:",fnumber
        print "feed_a:",feed_a
        print "feed_spacing:",feed_spacing
        print "Nfeeds:",Nfeeds1,'x',Nfeeds1
        print "At nominal z=",self.nz
        print "pix size angle [arcmin]: ", self.pix_size_ang*180./np.pi* 60. 
        print "pix size spatial [m]: ", self.pix_size_x
        print "lambda [m]: ", self.Lambda
    
    def Lambda_z(self,z):
        return 0.21*(1+z)

    def make_HE11_field(self, Lambda=None):
        if Lambda==None:
            Lambda=self.Lambda
        return bs.make_HE11_field(self.N, self.feed_a,100*self.feed_a, Lambda)


    def airyBeam(self,theta):
        lambdamax=3e2/self.freq[1]
        x=pi*4./lambdamax*theta.radian
        return (2*jn(1,x))**2/x**2

    def airyBeamImage (self,N, reso):
        """ reso in arcmin"""
        ones = np.ones(N)
        inds  = (np.arange(N)+.5 - N/2.)
        X = np.outer(ones,inds) * reso/(180.*60.)*np.pi
        Y = np.transpose(X)
        R = np.sqrt(X*X+Y*Y)
        lambdamax=3e2/self.freq[1]
        x=np.pi*4./lambdamax*R
        return (2*jn(1,x))**2/x**2

    def location(self):
        return EarthLocation(lat=40.87792*u.deg, lon=-72.85852*u.deg, height=0*u.m)

    def altaz(self, time):
        return AltAz(obstime=time,location=self.location(),alt=self.alt, az=self. az)

        

    
    def plotFeedBeam(self, z=None):
        if z==None:
            z=self.nz
        Lambda=self.Lambda_z(z)
        EHE11,Feed_beam,boresite_angle = self.make_HE11_field(Lambda)
        Feed_beam_log = 10.*np.log10(Feed_beam)
        N=self.N
        lo = N/2 - N/8
        hi = N/2 + N/8
        delta = 2.*((boresite_angle[N/2,N/2] - boresite_angle[N/2,lo]))

        max_beam_log = np.max(Feed_beam_log)
        bs.Plot_Map(Feed_beam_log[lo:hi,lo:hi],max_beam_log-6,max_beam_log,delta,delta)

        lo = N/2 -N/4
        hi = N/2 +N/4
        delta = 2.*((boresite_angle[N/2,N/2] - boresite_angle[N/2,lo]))
        bs.Plot_Map(Feed_beam_log[lo:hi,lo:hi],max_beam_log-60,max_beam_log,delta,delta)

        print "Wavelegnth [m]: ",Lambda
        print "feed diaemter [m]: ", 2*self.feed_a
        print "feed diaemter [wavelengths]: ", 2*self.feed_a / Lambda
        print "lambda / D [degrees]: ", 180./np.pi * (2*self.feed_a / Lambda)**-1 

    def simulate_beam_HE11feed(self, defocus,X_center,Lambda=None):
        if Lambda==None:
            Lambda=self.Lambda
        #print "Lambda here=",Lambda
        print "here",([self.N,self.pix_size,self.diam,self.F,Lambda,
                                      defocus,self.z_exit_aperature,X_center,self.feed_a])
        return bs.simulate_beam_HE11feed(self.N,self.pix_size,self.diam,self.F,Lambda,
                                      defocus,self.z_exit_aperature,X_center,self.feed_a)

    def simulate_one_beam_HE11feed(self, i,j, X_center,Lambda=None):
        """ simulate i,j beam, where they are integers, running from (-(self.Nfeeds1-1)/2..+(numer_of_feeds-1)/2) """
        if Lambda==None:
            Lambda=self.Lambda
        #edge_pos = -np.floor((self.Nfeeds1-1.)/2.)*self.feed_spacing
        defocus=np.array([i*self.feed_spacing, j*self.feed_spacing,0])
        return self.simulate_beam_HE11feed(defocus,X_center,Lambda)

    def simulate_all_beams_H11feed(self):
        mi=(self.Nfeeds1-1)/2
        beam_total=None
        for i in range(-mi,mi+1):
            for j in range(-mi,mi+1):
                print "i,j=",i,j
                magk2d, beam_uniform=self.simulate_one_beam_HE11feed(i,j,self.diam/1.3)
                if beam_total==None:
                    beam_total=beam_uniform
                else:
                    beam_total+=beam_uniform
        return beam_total

    def plotAllBeams(self, z=None):
        if z==None:
            z=self.nz
        Lambda=self.Lambda_z(z)
    
        N=self.N
        beam_total=self.simulate_all_beams_H11feed()
        lo = np.floor(N/2-170/(1.+z))
        hi = np.floor(N/2+170/(1.+z))
        print hi-lo
        delta = (hi - lo) *Lambda  / (N*self.pix_size) * 180./np.pi
        bs.Plot_Map(beam_total[lo:hi,lo:hi],0.,np.max(beam_total),delta,delta)

        lo = np.floor(N/2-300/(1.+z)/7.)
        hi = np.floor(N/2+300/(1.+z)/7.)
        print hi-lo
        delta = (hi - lo) *Lambda  / (N*self.pix_size) * 180./np.pi
        bs.Plot_Map(beam_total[lo:hi,lo:hi],0.,np.max(beam_total),delta,delta)


        log_beam = beam_total / np.max(beam_total)
        log_beam  = 10. * np.log10(log_beam)

        lo = np.floor(N/2-170/(1.+z))
        hi = np.floor(N/2+170/(1.+z))
        print hi-lo
        delta = (hi - lo) *Lambda  / (N*self.pix_size) * 180./np.pi
        bs.Plot_Map(log_beam[lo:hi,lo:hi],-50,0,delta,delta)

        plt.plot(log_beam[lo:hi,lo:hi])
        plt.xlabel("pixel [can be scaled to degrees]")
        plt.ylabel("beam power [dB]")
        plt.show()

    
    def plotOneBeam (self, z):
        Lambda=self.Lambda_z(z)
        magk2d,beam= self.simulate_one_beam_HE11feed(0, 0, self.diam/1.3, Lambda)
        ## a plot of the main beam
        N=self.N
        lo = np.floor(N/2-10)
        hi = np.floor(N/2+10)
        delta = (hi - lo) *self.beam_pix_size_arcmin/60.
        bs.Plot_Map(beam[lo:hi,lo:hi],0.,np.max(beam),delta,delta)
        ## a plot of the main beam and sidebobes in log space
        lo = np.floor(N/2-40)
        hi = np.floor(N/2+40)
        delta = (hi - lo) *self.beam_pix_size_arcmin/60.
        log_beam = beam / np.max(beam)
        log_beam  = 10. * np.log10(log_beam)
        bs.Plot_Map(log_beam[lo:hi,lo:hi],-50,0,delta,delta)
        ## an unrolled 1d Beam
        plt.plot(log_beam[lo:hi,lo:hi])
        plt.xlabel("pixel [can be scaled to degrees]")
        plt.ylabel("beam power [dB]")
        plt.show()
        
    
