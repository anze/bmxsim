#!/usr/bin/env python
#amodules
import sys,os
import numpy as np
from optparse import OptionParser
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

#our modules
sys.path=["py","../py"]+sys.path
from CrimeReader import CrimeReader

r=CrimeReader()

r.plot_slice(r.cosmo_slice(10),100,10)
r.plot_slice(r.gfree_slice(10),100,10)
r.plot_slice(r.egfree_slice(10),100,10)
#r.plot_slice(r.gsync_slice(10),100,10)
r.plot_slice(r.ptso_slice(10),100,10)

plt.show()
