#!/usr/bin/env python
import numpy as np
import sys,pylab
sys.path.append('py/')

import SingleDishTelescope as SDT
import astropy.units as u
from ObserveSky import ObserveSky
from CrimeReader import CrimeReader
import pickle
r=CrimeReader()
t=SDT.BMXDemo_SDT()
N=300
res=1.5
#im=t.airyBeamImage(240,6)
#pylab.imshow(im)
#pylab.show()
os=ObserveSky(t)
times=os.getTimeList(dt=15*u.s, Ns=240)
x=range(len(times))
if False:
    dl=[]
    for i, freq in enumerate(r.freq[:]):
        #sig=os.getIntegratedSignal(times,r.cosmo_slice(i),N,res)
        sig=os.getIntegratedSignal(times,r.ptso_slice(i),N,res)
        dl.append(sig)
    pickle.dump(dl,open('dlptso.dat','w'))
else:
    dl=np.array(pickle.load(open('dl.dat')))
    dlgf=np.array(pickle.load(open('dlgf.dat')))
    dlegf=np.array(pickle.load(open('dlegf.dat')))
    dlps=np.array(pickle.load(open('dlptso.dat')))
    pylab.figure(figsize=(10,10))
    pylab.subplot(2,2,1)
    pylab.ylabel("freq [GHz]")
    pylab.imshow(dl,interpolation='nearest',extent=(0,60,r.freq[0], r.freq[-1]),aspect='auto')
    pylab.title('signal')
    pylab.colorbar()
    pylab.subplot(2,2,2)
    pylab.imshow(dlgf,interpolation='nearest',extent=(0,60,r.freq[0], r.freq[-1]),aspect='auto')
    pylab.title('galactic free-free')
    pylab.colorbar()
    pylab.subplot(2,2,3)
    pylab.imshow(dlegf,interpolation='nearest',extent=(0,60,r.freq[0], r.freq[-1]),aspect='auto')
    pylab.title('extragalactic free-free')
    pylab.colorbar()
    pylab.xlabel("t[min]")
    pylab.ylabel("freq [GHz]")

    pylab.subplot(2,2,4)
    pylab.imshow(dlps,interpolation='nearest',extent=(0,60,r.freq[0], r.freq[-1]), aspect='auto')
    pylab.title('point sources')
    pylab.xlabel("t[min]")
    pylab.colorbar()
    pylab.savefig('sig.pdf')


    pylab.figure(figsize=(12,8))
    pylab.imshow(dl+dlgf+dlegf+dlps,interpolation='nearest',extent=(0,60,r.freq[0], r.freq[-1]), aspect='auto')
    pylab.title('all signals w/o monopole')
    pylab.xlabel("t[min]")
    pylab.colorbar()
    pylab.savefig('sigsum.pdf')
    
    
